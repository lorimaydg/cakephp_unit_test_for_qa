<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ArithmeticComponent; // required what component should be test
use Cake\Controller\ComponentRegistry; // required for initialization of component
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\ArithmeticComponent Test Case
 */
class AddTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\ArithmeticComponent
     */
    public $Arithmetic; // component name for global variable

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry(); // required for initialization of component
        $this->Arithmetic = new ArithmeticComponent($registry); // code for initialization of component to be tested
    }

    /**
     * Test initial setup
     * Scenario: test add method with empty parameters
     * 
     * Expected : 0
     * @return void
     */
    public function testEmptyParameters()
    { 
        // call add function in AritmeticComponent store in $actual variable 
        $actual = $this->Arithmetic->add();
        // assert if $actual is equal to expected result
        $this->assertEquals(0, $actual);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Arithmetic);

        parent::tearDown();
    }
}
